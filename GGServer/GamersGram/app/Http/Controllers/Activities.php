<?php

namespace App\Http\Controllers;
use App\Models\Block;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
session_start();
if(!isset($_SESSION['login']))
{
    header("Location:admin");
}
use App\Models\Blocked;
use App\Models\User;
use App\Models\Token;
use App\Models\Contest;
use App\Models\Spin;
use App\Models\Matche;
use App\Models\Result;
use Illuminate\Http\Request;
use App\Models\Bot;
 use App\Models\Paymentrequest;
 use App\Models\Bankdetail;
 use App\Models\Contestjoin;
 use App\Models\Contestrank;
 use App\Models\Randuser;

class Activities extends Controller
{
    function blockUser(Request $req)
    {
        $opt = User::where('id',$req->id)->update(['isBlocked'=>1]);
        $usr = User::where('id',$req->id)->first();
        if($opt)
        {
            Blocked::insert(['id'=>$req->id,'activity'=>$req->activity,'reason'=>$req->reason]);

            $block = Block::where('devId',$usr->deviceId)->first();

            if(!$block){
                Block::insert(["devId"=>$usr->deviceId]);
            }
            
            header('Location:users');
        }
    }

    public function AddUsersBulk(Request $request)
    {
        

        $validator = Validator::make($request->all(),[
            'sheet' => 'required|max:5000|mimes:xlsx,xls,csv'
        ]);

        if(true)
        {
            $dateTime = date('Ymd_His');
            $file = $request->file('sheet');
            $fileName = $dateTime . '-' .$file->getClientOriginalName();
            $savePath = public_path('/uploads/');
            $file->move($savePath,$fileName);
            $rowCount = 0;
            $startWith = 0;
            $rand = Randuser::select('*')->orderBy('created_at','desc')->limit(1)->first();
            if($rand)
                $startWith = $rand->uid;
            if (($handle = fopen ( $savePath.$fileName, 'r' )) !== FALSE) {
                while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {

                    if($rowCount > 0)
                    {
                        $startWith -= 1;
                        $defVal = rand(0,2);
                        Randuser::insert(["uid"=>$startWith,"name"=>$data[0],"prf"=>"https://ludo.ludootime.in/profilePictures/default".$defVal.'.png']);
                        
                    }

                    $rowCount++;
                }
                fclose ( $handle );
            }
            //dd("passes");
            return redirect()->back();
        }
        
    }

    function SearchUser(Request $request)
    {
        $user = User::where('phone',$request->ph)->first();

        $resString = "<tr>No Data Found</tr>";
        if($user){
            $allPayments = 0;
            $allWithdrawn = 0;
            $payments = Payment::where('userName',$user->userName)->get();
            $withdrawn = Paymentrequest::where('userId',$user->id)->get();
            foreach($payments as $payment)
                                {
                                  $allPayments += $payment->amount;
                                }

                                foreach($withdrawn as $withdraw)
                                {
                                  $allWithdrawn += $withdraw->amount;
                                }                                  
            $online = "";                     
            if($user->isOnline == 0)
                $online = "No";
            else
                $online = "Yes";                     
                
                if($user->isBlocked == 0)
                {
                    $resString = "<tr id='".$user->phone."'><td>".$user->id."</td><td>".$user->name."</td><td>".$user->userName."</td><td>".$user->email."</td><td>".$user->phone."</td><td>".$user->createdAt."</td><td>".$user->lastLogin."</td><td>".$user->bounty."</td><td>".$user->referal."</td><td>".$online."</td><td>".$user->totalMatch."</td><td>".$user->totalWon."</td><td>".$user->refferd."</td><td>".$allPayments."</td><td>".$user->winBounty."</td><td>".$allWithdrawn."</td><td><div class='text-center'>
                    <a href='' class='btn btn-warning btn-rounded mb-4' data-toggle='modal' data-target='#modalRegisterForm' onclick='blockUser(".$user->id.")'>Block</a> <a href='' class='btn btn-success btn-rounded mb-4' data-toggle='modal' data-target='#modalAddmoney' onclick='blockUser(".$user->id.")'>Add Money</a> <a href='' class='btn btn-success btn-rounded mb-4' data-toggle='modal' data-target='#modalAddmoneyWithdraw' onclick='blockUser(".$user->id.")'>Add Money Winnings</a>
                  </div>";
                }
                else
                {
                    $resString = "<tr id='".$user->phone."'><td>".$user->id."</td><td>".$user->name."</td><td>".$user->userName."</td><td>".$user->email."</td><td>".$user->phone."</td><td>".$user->createdAt."</td><td>".$user->lastLogin."</td><td>".$user->bounty."</td><td>".$user->referal."</td><td>".$online."</td><td>".$user->totalMatch."</td><td>".$user->totalWon."</td><td>".$user->refferd."</td><td>".$allPayments."</td><td>".$user->winBounty."</td><td>".$allWithdrawn."</td><td><div class='text-center'>
                    <a href='unblock?id=".$user->id."' class='btn btn-danger btn-rounded mb-4'>Unblock</a>
                  </div>";
                }

                if($user->isInfluencer == 0)
                {
                    $resString .= "<div class='text-center'>
                    <a href='promote?id=".$user->id."' class='btn btn-info btn-rounded mb-4'>Promote</a>
                  </div></td></tr>";
                }
                else{
                    $resString .= "<div class='text-center'>
                    <a href='demote?id=".$user->id."' class='btn btn-danger btn-rounded mb-4'>Demote</a>
                  </div></td></tr>";
                }
                                      




        }

        return response($resString,200);


        
        
    }

    function unblockUser(Request $req)
    {
        //die($req->id);
        $opt = User::where('id',$req->id)->update(['isBlocked'=>0]);

        if($opt)
        {
           Blocked::where('id',$req->id)->delete();
           header('Location:users');
        }
    }

    function AcceptPayment(Request $request)
    {
        $payment = Paymentrequest::where('SN',$request->id)->first();
       Paymentrequest::where('SN',$request->id)->update(['status'=>1]);
       $phone = User::where('id',$payment->userId)->first()->phone;
       $name = User::where('id',$payment->userId)->first()->name;
      
       header('Location:activity');
    }

    function UpdateWithdraw()
    {
       //$run = DB::statement('UPDATE users SET withdraw=2');
       User::where('withdraw',0)->update(['withdraw'=>1]);
       return redirect()->back();
    }

    function SendSms($msg,$number)
    {
        $params=array(
            
            'route ' => 'q',
            'sender_id ' => 'FTZSMS',
            'message'=>$msg,
            
            'numbers'=>$number,
            
            );
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://www.fast2sms.com/dev/bulkV2",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => http_build_query($params),
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "authorization"=>"K3CgpXpvkr"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);

            return response($response);
    }

    function DeclinePayment(Request $request)
    {
        $payment = Paymentrequest::where('SN',$request->id)->first();
        Paymentrequest::where('SN',$request->id)->update(['status'=>2]);
        $winbounty = User::where('id',$payment->userId)->first()->winBounty;
        $refund = $payment->amount + $winbounty;
        User::where('id',$payment->userId)->update(['winBounty'=>$refund]);
        //$this->sendSmsToUser($payment->userId,"rejected");
        header('Location:activity');
    }

    function GetRandBots()
    {
       
    }

    function CreateContest(Request $request)
    {
        $usr = new Users();
        $token = $usr->generateRandomString(15);
        $distribute = $request->amount * 50 /100;
        $distribution = new Distribution();
        $distribution->Distribute($request->amount,$request->people,$token);
        $defaultFill = 0;

        $fillPercent = 50;
        if($request->people > 50000)
        {
            $fillPercent = rand(35,55);
        }
        else if($request->people > 30000)
        {
            $fillPercent = 30;
        }
        else if($request->people > 20000)
        {
            $fillPercent = 30;
        }
        else if($request->people > 10000)
        {
            $fillPercent = 30;
        }
        else
        {
            $fillPercent = 30;
        }

        $defaultFill = $request->people * $fillPercent / 100;
        $selectedBot = [];

        $allRands = Randuser::orderByRaw("RAND()")->limit(100)->get();
        $minLimit = 0;
        $maxLimit = 0;

        if($request->type == "Ludo")
        {
            $maxLimit = 300;
            $minLimit = 271;
        }
        else
        {
            $maxLimit = 1500;
            $minLimit = 1400;
        }
       $rankCount = 1;
        
        Contest::insert(["type"=>$request->type,"people"=>$request->people,"entry"=>$request->entry,"distribute"=>$distribute,"amount"=>$request->amount,"endtime"=>$request->end,"token"=>$token,"defaultFill"=>$defaultFill]);

        foreach($allRands as $ar)
       {
        $randScore = rand($minLimit,$maxLimit);
         Contestjoin::insert(["contestId"=>$token,"userId"=>$ar->uid,"ranking"=>$rankCount,"score"=>$randScore]);
         $rankCount++;
       }

        return redirect()->back();

    }

    

    function sendSmsToUser($uid,$type)
    {
        $phone = User::where('id',$uid)->first()->phone;
        $name = User::where('id',$uid)->first()->userName;
        
        if($type=="accept")
        {
            $text = "Hi ". $name.",Your withdrawal request have been accepted! Thanks from Todd apples.";
        }
        else
        {
            $text = "Hi ". $name.",Your payment withdraw request have been rejected! Thanks from Todd apples.";
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "http://sms.leariesservices.com/api/mt/SendSMS?user=Gamersgram&password=12345&senderid=TETENN&channel=Trans&DCS=0&flashsms=0&number=".$phone."&text=".$text);

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
curl_close($curl);
return $output;
    }

    function enableOrDiableToken(Request $request)
    {
       $dat = Token::where('SN',$request->id)->first();

       if($dat->status == 0)
       {
           Token::where('SN',$request->id)->update(['status'=>1]);
           header("Location:activity");
       }
       else
       {
           Token::where('SN',$request->id)->update(['status'=>0]);
           header("Location:activity");
       }
    }

    function updateSpinValues(Request $request)
    {
        $update = Spin::where("SN",1)->update(["zero"=>$request->one,"one"=>$request->two,"two"=>$request->three,"three"=>$request->four,"four"=>$request->five,"five"=>$request->six]);
        if($update)
        header("location:activity");
    }
    function AddBots(Request $request){
        
        $count = 1;
        $val = $count.'name';
        $pic = $count.'picture';
        while(!empty($request->$val) && !empty($request->$pic))
        {
            //$picture = $request->$pic;
            //print_r($request->$val.$request->$pic);exit(0);
            //$imageName = $request->$val.'.png';
            $imageSaveStat = $this->getProfImage($request,$request->$val,$pic);
            //$picture->move(public_path('profilePictures'), $imageName);
            $profUrl = url('/profilePictures/'.$request->$val.'.png?'.rand(10000000,900000000));

        if(!$imageSaveStat)
        {
            $profUrl = url('/profilePictures/default.png');
        }
            Bot::insert(['userName'=>$request->$val,'profilePic'=>$profUrl]);
            $count++;
            $val = $count.'name';
            $pic = $count.'picture';
        }

        header("Location:activity");
    }
    function getProfImage(Request $image,$userName,$picName)
    {
       // $base64Encoded = $image;
        
            
              $usrName = $userName;
              $poc =  \Image::make($image->file($picName)->getRealPath())->resize(64,64)->save(public_path('profilePictures/'.$usrName.'.png'));
              if($poc)
              {
               return true;
               }
               return false;
            
            
            
    }
    
}
