<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Other;
use App\Models\Otp;
use Exception;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\TransactionHistory;
use App\Models\Token;
use App\Models\Spin;
use App\Models\Matche;
use App\Models\Payment;
use App\Models\Temppayment;
use App\Models\Result;
use App\Models\Vpa;
use App\Models\Bankdetail;
use App\Models\Paymentrequest;
use App\Models\Version;
use App\Models\Bot;
use App\Models\Transaction;
use App\Models\Upiset;
use App\Models\Rummystat;
use App\Models\Contest;
use App\Models\Contestjoin;
use App\Models\Contestrank;
use App\Models\Randuser;
use App\Models\Contestresult;

use View;
use Illuminate\Support\Facades\Http;
class Users extends Controller
{
    public $hostUrl = "https://payout-api.cashfree.com";
    //public $hostUrl = "https://payout-gamma.cashfree.com";
    function CheckHeader(Request $request)
    {
        if($request->header('app-auth')=='gg_ludo_8080')
        {
            return true;
        }

        return false;
    }

    public function UpdateAllContest()
    {
        $contest = Contest::where('status','running')->get();
        if($contest)
        {
            foreach($contest as $cont)
            {
                $endTime = date('Y-m-d H:i:s',(strtotime($cont->created_at.' + '.$cont->endtime.' hours')));
                $nowTime = date('Y-m-d H:i:s');

                if(strtotime($nowTime) > strtotime($endTime))
                {
                    Contest::where('id',$cont->id)->update(['status'=>'complete']);
                }
            }
        }

        return;
    }

    public function GetResult(Request $request,$forrefresh = false)
    {
        $contest = Contest::where('token',$request->token)->first();
        $dist = new Distribution();
        $listing = $dist->Distribute(round($contest->amountCollected/2),round($contest->totalJoined/2));
        $maxAmountToDis = round($contest->amountCollected/2);
        $totalAmountDisted = 0;
        $allLists = explode('|',$listing);
        //$halfUsers = '';
        if($contest->totalJoined > 1)
            $halfUsers = Contestjoin::where('contestId',$request->token)->where('userId','>',0)->where('score','>',0)->orderBy("score","desc")->limit(round($contest->totalJoined/2))->get();
        else
            $halfUsers = Contestjoin::where('contestId',$request->token)->where('userId','>',0)->where('score','>',0)->orderBy("score","desc")->get();
        
        $contestRanks = Contestrank::where('contestId',$request->token)->first();
        $lastRankGot = $contestRanks->lastRank;
        $contestRanks = $contestRanks->allRankPrizes;
        $apr = explode('|',$contestRanks);
        $pListing = "";
        $count = 0;
        $totalAmount = 0;
        $rankUsed = [];
        
        
        Contestresult::where('contestId',$request->token)->delete();
        foreach($halfUsers as $half)
        {
            $usr = User::where('id',$half->userId)->first();
            $foundPrize = false;
            if($totalAmountDisted < $maxAmountToDis)
            {
                foreach($allLists as $al)
                {
    
                    if($al != '' && $al !=  null){
    
                        $priceRank = explode(':',$al);
    
                    foreach($apr as $ap)
                    {
                        if($ap != '' && $ap !=  null)
                        {
                            $priceRankGet = explode(':',$ap);
    
                            if($priceRank[1] >= $priceRankGet[1])
                            {
                                $getRank = 0;
                                if(str_contains($priceRankGet[0],'-'))
                                {
                                    while(true)
                                    {
                                        $getRank = rand(explode('-',$priceRankGet[0])[0],explode('-',$priceRankGet[0])[1]);
                                        if(!in_array($getRank,$rankUsed))
                                        {
                                            array_push($rankUsed,$getRank);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    $getRank = $priceRankGet[0];
                                }
                                Contestjoin::where('contestId',$request->token)->where('userId',$half->userId)->update(["ranking"=>$getRank]);
                                Contestresult::insert(['contestId'=>$request->token,'userId'=>$half->userId,'rank'=>$getRank,'prize'=>$priceRankGet[1]]);
                                $totalAmountDisted += $priceRankGet[1];
                                $pListing .= $usr->name.':'.$getRank.':'.$priceRankGet[1].'|';
                                $totalAmount += $priceRankGet[1];
                                $foundPrize = true;
                                //die("Coming here one");
                                break;
                            }
                        }
    
                        
                    }
                    }
    
                    
                    if($foundPrize)
                    {
                        //die("Coming here sec");
                        break;
                    }
                    
                }
            }
            

            if(!$foundPrize)
            {
                    $lastRankGot = $lastRankGot + 1;
                    //die("Coming here last ".$lastRankGot);
                    Contestjoin::where('contestId',$request->token)->where('userId',$half->userId)->update(["ranking"=>$lastRankGot]);
            }
        }
        if($pListing == "")
        {
            $pListing = "NA:NA:NA|";
        }


        if(!$forrefresh)
            return response($pListing,200);
        else
            return;

    }

    public function ContestRefresh()
    {
        $allContest = Contest::where('status','running')->get();

        foreach($allContest as $ac)
        {
            $request = new Request(["token"=>$ac->token]);
            $this->GetResult($request,true);
        }

        return response("done",200);
    }

    public function PublishResult(Request $request)
    {
        Contest::where('token',$request->token)->update(["status"=>"over"]);
        $constestResult = Contestresult::where('contestId',$request->token)->where('status','ns')->get();

        foreach($constestResult as $csr)
        {
           $usr = User::where('id',$csr->userId)->first();
           User::where('id',$csr->userId)->update(["bounty"=>($usr->bounty+$csr->prize)]);
           Contestresult::where('contestId',$request->token)->where('userId',$csr->userId)->update(["status"=>"cleared"]);
           $this->InitTransaction($csr->prize,$csr->userId,"Contest Winning Amount");
        }

        return redirect()->back();
    }

    public function GetPastContests(Request $request)
    {
        $contest = Contest::where('status','over')->orderBy('amount','desc')->limit(6)->get();
        
        foreach($contest as $cont)
        {
            $prize = Contestrank::where('contestId',$cont->token)->first();
            $hasUser = Contestjoin::where('contestId',$cont->token)->where('userId',$request->id)->first();
            $leaderBoards = Contestjoin::where('contestId',$cont->token)->orderBy('score','desc')->limit(100)->get();
            foreach($leaderBoards as $lead)
            {
                if($lead->userId > 0)
                {
                    $user = User::where('id',$lead->userId)->first();
                    $lead["prfPath"] = $user->profilePhotoPath;
                    $lead["name"] = $user->name;
                }
                else
                {
                    $user = Randuser::where('uid',$lead->userId)->first();
                    $lead["prfPath"] = $user->prf;
                    $lead["name"] = $user->name;
                }
            }
            $cont["Prizes"] = $prize->allRankPrizes;
            $cont["leader"] = $leaderBoards;

            if($hasUser)
            {
                $cont["hasJoined"] = "yes";
                $cont["myRank"] = $hasUser->ranking;
                $cont["myScore"] = $hasUser->score;
            }
            else
            {
                $cont["hasJoined"] = "no";
                $cont["myRank"] = "NA";
                $cont["myScore"] = "0";
            }

            $cont["startTime"] = explode(' ',$cont->created_at)[0]." ".explode(' ',$cont->created_at)[1];
            $endTime = date('Y-m-d H:i:s',(strtotime($cont->created_at.' + '.$cont->endtime.' hours')));
            $resultTime = date('Y-m-d H:i:s',strtotime($endTime.' + 15 minutes'));
            $cont["endTime"] = $endTime;
            $cont["resultTime"] = $resultTime;


        }
        
        //$contest["Prizes"] = $prize->allRankPrizes;
        return response([
            "response_code"=>200,
            "response_msg"=>"Fecthed all contests",
            "response_data"=>$contest
        ],200);
    }

    public function GetAllContests(Request $request)
    {

        $this->UpdateAllContest();

        $contest = Contest::where('status','running')->orderBy('amount','desc')->limit(6)->get();
        
        foreach($contest as $cont)
        {
            $prize = Contestrank::where('contestId',$cont->token)->first();
            $hasUser = Contestjoin::where('contestId',$cont->token)->where('userId',$request->id)->first();
            $leaderBoards = Contestjoin::where('contestId',$cont->token)->orderBy('score','desc')->limit(100)->get();
            foreach($leaderBoards as $lead)
            {
                if($lead->userId > 0)
                {
                    $user = User::where('id',$lead->userId)->first();
                    $lead["prfPath"] = $user->profilePhotoPath;
                    $lead["name"] = $user->name;
                }
                else
                {
                    $user = Randuser::where('uid',$lead->userId)->first();
                    $lead["prfPath"] = $user->prf;
                    $lead["name"] = $user->name;
                }
            }
            $cont["Prizes"] = $prize->allRankPrizes;
            $cont["leader"] = $leaderBoards;

            if($hasUser)
            {
                $cont["hasJoined"] = "yes";
                $cont["myRank"] = $hasUser->ranking;
                $cont["myScore"] = $hasUser->score;
            }
            else
            {
                $cont["hasJoined"] = "no";
                $cont["myRank"] = "NA";
                $cont["myScore"] = "0";
            }

            $cont["startTime"] = explode(' ',$cont->created_at)[0]." ".explode(' ',$cont->created_at)[1];
            

            $endTime = date('Y-m-d H:i:s',(strtotime($cont->created_at.' + '.$cont->endtime.' hours')));
            $resultTime = date('Y-m-d H:i:s',strtotime($endTime.' + 15 minutes'));
            $cont["endTime"] = $endTime;
            $cont["resultTime"] = $resultTime;

            $endingOn = strtotime($cont->created_at.' + '.$cont->endtime.' hours');
            $nowOn = strtotime(date('Y-m-d H:i:s'));

            $inMinutes = ($endingOn - $nowOn) / 60;
            $cont->endtime = $inMinutes;


        }
        
        //$contest["Prizes"] = $prize->allRankPrizes;
        return response([
            "response_code"=>200,
            "response_msg"=>"Fecthed all contests",
            "response_data"=>$contest
        ],200);
    }

    public function JoinContest(Request $request)
    {
        $contest = Contest::where('token',$request->token)->first();
        $usr = User::where('id',$request->id)->first();

        
        if($contest)
        {
            if($contest->status == "running")
            {


                if($usr->bounty < $contest->entry)
                {
                    return response("You dont have enough money in your wallet! Add some cash",401);
                }
                User::where('id',$request->id)->update(["bounty"=>($usr->bounty-$contest->entry)]);
                $cj = Contestjoin::where('userId',$request->id)->where('contestId',$request->token)->first();
                if(!$cj)
                {
                    Contestjoin::insert(["contestId"=>$request->token,"userId"=>$request->id]);
                    Contest::where('token',$request->token)->update(["totalJoined"=>($contest->totalJoined + 1),"amountCollected"=>($contest->amountCollected + $contest->entry)]);
                }
                else
                {
                    Contest::where('token',$request->token)->update(["amountCollected"=>($contest->amountCollected + $contest->entry)]);
                }
                

                

                return response("Joined",200);
            }
            return response("Contest is expired",401);
        }
        return response("Invalid request",401);
    }

    public function GetMyRanking(Request $request)
    {
      $allParticipants =  Contestjoin::where('contestId',$request->contest)->where('userId','>',0)->orderBy('score','desc')->get();
      $contest = Contest::where('token',$request->contest)->first();
        $myRank = 0;
        $counter = 0;
        $goneForReal = false;
      if(count($allParticipants) > 0)
      {
        $counter++;

        foreach($allParticipants as $particiapant)
        {
            if($particiapant->userId != $request->id){
                if($particiapant->score < $request->score)
                {   
                    $myRank = $particiapant->ranking - 1;
                   // echo("participant one");
                    $goneForReal = true;
                    break;
                }
                else if($particiapant->score == $request->score)
                {
                    

                    for($i=$counter;$i<count($allParticipants);$i++)
                    {
                        if($allParticipants[$i]->score < $request->score)
                        {
                            if($allParticipants[$i]->ranking < $contest->people)
                            {
                                $myRank = $allParticipants[$i]->ranking - 1;
                            // echo("participant two");
                             $goneForReal = true;
                                break;
                            }
                        }
                    }

                    break;
                    
                }
            }
            
        }
      }


      if(!$goneForReal)
      {
        $rank = Contestrank::where('contestId',$request->contest)->first();
       
        $allRanksNow = explode('|',$rank->allRankPrizes);
        foreach($allRanksNow as $ar)
        {
           if($ar != null && $ar != "")
           {
            $ars = explode(':',$ar);
            if($ars[1] <= ($contest->entry * 50 / 100))
            {
                $ranksGot = explode('-',$ars[0]);

                $myRank = rand($ranksGot[0],$ranksGot[1]);
                $goneForReal = true;
                break;
            }
           }
        }

        if(!$goneForReal){
            $startFrom = round($rank->lastRank);
       // echo("participant three stfrom ".$startFrom);
            $myRank = $startFrom;
        }
        
      }
      $usr = Contestjoin::where('userId',$request->id)->where('contestId',$request->contest)->first();
      if($usr->score < $request->score)
      {
        Contestjoin::where('userId',$request->id)->where('contestId',$request->contest)->update(["ranking"=>$myRank,"score"=>$request->score]);
      }

      $usr = Contestjoin::where('userId',$request->id)->where('contestId',$request->contest)->first();

      return response($usr->ranking.'|'.$usr->score,200);

    }

    public function GetRummystat(Request $request)
    {
       $rs = Rummystat::where('version', $request->version)->first();
       if($rs->status == "yes")
       {
        return response("yes",200);
       }

       return response("no",200);
    }

    public function GetWhatsappData(Request $request)
    {
        
        $this->VerifyNumber($request);
        return response("done",200);
    }

    public function RegisterOtp(Request $request)
    {
        $token = $this->generateRandomString(15);
        $newnum = "91".$request->phone;
        Otp::insert(["phone"=>$newnum,"token"=>$token,"status"=>"not approved"]);
        return response($token ,200);
    }
    public function VerifyNumber(Request $request)
    {
        $needle = "please verify my number";
        
        $num = Otp::where("phone",$request->phone)->orderBy("created_at","desc")->limit(1)->first();

        
        if(str_contains(strtolower($request->message),$needle))
        {
            if($num)
            {
                if($num->status == "not approved")
                {
                    Otp::where("phone",$request->phone)->update(["status"=>"approved"]);
                    $request = new Request(["phone"=>$request->phone,"message"=>"your phone number has been verified. click on this link to login in Ludo Time https://gamingstok.ludootime.in/approve?token=".$num->token." to approved request"]);

                        $this->SendMessage($request);
                }
                else
                {
                    $request = new Request(["phone"=>$request->phone,"message"=>'This is not the number you used to sign up in Ludo Time game. Please send this message " please verify my number " from the number you used for registration in game.']);

                    $this->SendMessage($request);
                }
            }
            else
            {
                $request = new Request(["phone"=>$request->phone,"message"=>'This is not the number you used to sign up in Ludo Time game. Please send this message " please verify my number " from the number you used for registration in game.']);

                    $this->SendMessage($request);
            }
        }
        else
        {
            die("Does not contains");
        }
    }

    public function SendMessage(Request $request)
    {
        $params=array(
            'token' => 'nd7idvg5fr0aznef',
            'to' => $request->phone,
            'body' => $request->message
            );
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.ultramsg.com/instance65504/messages/chat",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => http_build_query($params),
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);

            //Chat::insert(["phone"=>$request->phone,"message"=>$request->message,"sendBy"=>"me"]);

            return response("Sent",200);
    }

    function GamingStokRummy()
    {
        return response("yes",200);
    }

    function UpdateUpi(Request $request){
        Upiset::where('id',1)->update(["upi"=>$request->upi]);
        return redirect()->back()->with('success', 'upi updated');
    }

    function UpdateVersion(Request $request)
    {
        Version::where('SN',1)->update(["currentVersion"=>$request->version]);
        return redirect()->back()->with('success', 'version updated');
    }

    function AddMoney(Request $request)
    {
        $usr = User::where('id',$request->id)->first();
        $totalBal = $usr->bounty + $request->amount;
        User::where('id',$request->id)->update(["bounty"=>$totalBal]);
        $this->InitTransaction($request->amount,$usr->id);
        return redirect()->back()->with('success', 'Amount added');
    }

    function AddMoneyWithdraw(Request $request)
    {
        $usr = User::where('id',$request->id)->first();
        $totalBal = $usr->winBounty + $request->amount;
        User::where('id',$request->id)->update(["winBounty"=>$totalBal]);
        $this->InitTransaction($request->amount,$usr->id);
        return redirect()->back()->with('success', 'Amount added');
    }

    function getElegibility(Request $request)
    {
        $usr = User::where('id',$request->id)->first();
        $totalBal = $usr->bounty;
        return response([
            "response_code"=>"200",
            "response_msg"=>"elegibility fetched",
            "response_data"=>$totalBal
            ]);
    }

    function webRegistration(Request $request)
    {
        $data = ["refer"=>$request->refer];
        return view('Admin/Pages/webRegister',$data);
    }

    function GetUserTransactions(Request $request)
    {
        $user = User::where('id',$request->userId)->first();
        $trans = Transaction::where('id',$user->id)->orderBy('createdAt','desc')->get();

        return response([
            "response_code"=>"200",
            "response_msg"=>"all transactions",
            "response_data"=>$trans
            ],200);
    }

    function getBotsDetails()
    {
        $bots = Bot::where('status',0)->get();
        $users = User::select("*")->orderBy("winBounty","desc")->limit(200)->get();

        return response([
            "response_code"=>"200",
            "response_data"=>$bots,
            "response_msg"=>"fetched"
            ]);
    }

    function giveSpin()
    {
        $users = User::select('*')->get();

        foreach($users as $usr)
        {
            User::where('id',$usr->id)->update(['spin'=>1]);
        }
    }

    function updateSpin(Request $request)
    {
        //$user = User::where('id',$request->id)->first();

        User::where('id',$request->id)->update(['spin'=>$request->spin]);
        return response("done",200);
    }

    function InitTransaction($amt,$id,$type="Wallet Addition")
    {
         $req = new Request(["amount"=>$amt,"id"=>$id,"type"=>$type]);
         //$req->amount = $amt;
         //$req->id = $id;
         //$req->type="WA";
         $tran = new TransactionHistory();

        return $tran->setTransaction($req);
    }


    function SetPaymentNow(Request $request)
    {
        $cashfreeData = json_decode($request->getContent(), true);

        //return response($cashfreeData["data"]["customer_details"]["customer_id"],200);

        $orderId = $cashfreeData["data"]["order"]["order_id"];
        $paystat = $cashfreeData["data"]["payment"]["payment_status"];

        if($paystat != "SUCCESS")
        {
            return response("No done",200);
        }

        $tempPay = Temppayment::where('orderId',$orderId)->first();
        if($tempPay)
        {
            $getId = Payment::where('orderId',$orderId)->first();
            if($getId)
            {
                return response("Repeated",200);
            }
            else
            {
                $user=User::where('id',$tempPay->userId)->first();
                $ins = Payment::insert(["userName"=>$user->userName,"type"=>"added","status"=>"success","amount"=>$tempPay->amount,"orderId"=>$tempPay->orderId]);

                $percentAmount = 0;
                

                

                if($tempPay->amount >= 1000 || $tempPay->amount <= 2000)
                {
                    $percentAmount = $tempPay->amount * 40 / 100;
                }
                else if($tempPay->amount >= 500)
                {
                    $percentAmount = $tempPay->amount * 10 / 100;
                }
                else if($tempPay->amount >= 100 || $tempPay->amount < 500)
                {
                    $percentAmount = $tempPay->amount * 5 / 100;
                }
                else if($tempPay->amount >= 75 || $tempPay->amount < 100)
                {
                    $percentAmount = $tempPay->amount * 2 / 100;
                }

                $nowAmt = $user->bounty + $tempPay->amount;
                $bonusAmt = $user->bonus + $percentAmount;
                $update = User::where('id',$tempPay->userId)->update(["bounty"=>$nowAmt,"bonus"=>$bonusAmt]);
                if($update){
                    $this->InitTransaction($tempPay->amount,$tempPay->userId,"Cashfree wallet addition");
                    return response("Done",200);
                }
                else
                    return response("Failed",200);
            }
        }

    }
    
    function setPaymentThroughCashfree(Request $request)
    {
        //csrf_field();
        //die($request);
//         $tempPay = Temppayment::where('orderId',$request->orderId)->first();
//         if($tempPay){
//         $getId = Payment::where('orderId',$request->orderId)->first();
//         if($getId)
//         {
//             header('location:paymentStat?type=repeat');
//         }
//         else
//         {
//             $url = 'https://test.cashfree.com/api/v1/order/info/status';
// // Collection object
// $data = [
//   'appId' => '88158e67f7fb775e6ff36b3c485188',
//   'secretKey'=>'d4255cb85af13f4654909f325eadc93912a97754',
//   'orderId'=>$request->oid

// ];
// // Initializes a new cURL session
// $curl = curl_init($url);
// // Set the CURLOPT_RETURNTRANSFER option to true
// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// // Set the CURLOPT_POST option to true for POST request
// curl_setopt($curl, CURLOPT_POST, true);
// // Set the request data as JSON using json_encode function
// curl_setopt($curl, CURLOPT_POSTFIELDS,  $data);
// // Set custom headers for RapidAPI Auth and Content-Type header
// curl_setopt($curl, CURLOPT_HTTPHEADER, [
//   'Content-Type: application/x-www-form-urlencoded'
// ]);
// // Execute cURL request with all previous settings
// $response = curl_exec($curl);

// $parsedData = json_decode($response);

// // Close cURL session
// curl_close($curl);
// //die(print_r($parsedData));

// if($request->txStatus == "SUCCESS")
// {
//     $user=User::where('id',$tempPay->userId)->first();
//     $ins = Payment::insert(["userName"=>$user->userName,"type"=>"added","status"=>"success","amount"=>$request->orderAmount,"orderId"=>$request->orderId]);
    
//     $nowAmt = $user->bounty + $request->orderAmount;
//     $update = User::where('id',$tempPay->userId)->update(["bounty"=>$nowAmt]);
//     if($update){
//         $this->InitTransaction($request->orderAmount,$tempPay->userId,"Cashfree wallet addition");
//     header('location:paymentStat?type=paid');
//     }
//     else
//         header('location:paymentStat?type=declined');
// }
// else
// {
//     header('location:paymentStat?type=declined');
// }

//         }
//         }
    }

    function setTempPay(Request $request)
    {

        

        $minAmt = Other::where('id',1)->first();
        if($request->amount < $minAmt->value)
        {
            return response([
                "response_code"=>"203",
                "response_data"=>"Failed",
                "response_msg"=>"Minimum 25 rs should be added"
                ],203);
        }

        $ins = Temppayment::insert(["userId"=>$request->userId,"orderId"=>$request->orderId,"amount"=>$request->amount]);
        if($ins)
        {
            return response([
                "response_code"=>"200",
                "response_data"=>"success",
                "response_msg"=>"temp payment is made successfully"
                ],200);
        }
    }

    function checkVersion()
    {
        $version = Version::select('*')->first();

        return response($version->currentVersion,200);
    }

    function getWalletUpdate(Request $request)
    {
        $dat = User::where('id',$request->id)->first();
        

        if($dat)
        {
            return response([
                "response_code"=>"200",
                "response_data"=>$dat->bounty,
                "response_msg"=>"wallet details"
                ],200);
        }
    }

    function promoteUser(Request $request)
    {
        $user = User::where('id',$request->id)->first();

        if($user)
        {
            User::where('id',$request->id)->update(["isInfluencer"=>1]);
        }
        header('Location:users');
    }
    function demoteUser(Request $request)
    {
        $user = User::where('id',$request->id)->first();

        if($user)
        {
            User::where('id',$request->id)->update(["isInfluencer"=>0]);
        }
        header('Location:users');
    }

    function Login(Request $request)
    {
        //$uri = "https://www.fast2sms.com/dev/bulkV2";
       $auth = $this->CheckHeader($request);


       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

       $data = User::where('phone',$request->data)->first();
       $login = Upiset::where('id',1)->first();
       $minAmt = Other::where('id',1)->first();
       
       if($data)
       {
        if($data->alias == null || $data->alias == "")
        {
            $name =  substr($request->data,0,2) . "***".substr($request->data,5,9);
            $alias = $data->name;
            User::where('phone',$request->data)->update(["name"=>$name,"alias"=>$alias]);
            $data = User::where('phone',$request->data)->first();
        }
        $data["upi"] = "none@ybl";
        $data["minAmount"] = $minAmt->value;
          return response([
            'response_code'=>'200',
            'response_msg'=>'valid',
            'response_data'=>$data
            ],200);
       }
       else
       {

        $rands = rand(0,2);

        $profUrl = url('/profilePictures/default'.$rands.'.png');
        $usrName = "GTS". $this->generateRandomString(12);
        $name =  substr($request->data,0,2) . "***".substr($request->data,5,9);
        $alias = "GTS". $this->generateRandomName(12);
        $refBy = null;
        if(!empty($request->referal))
        {
            $refBy = $request->referal;
        }
        $email = "none".substr($request->data,5,9)."@email.com";
            $done = User::insert([
            "name"=> $name,
            "alias"=>$alias,
            "email"=>$email,
            "userName"=>$usrName,
            "phone"=>$request->data,
            "bounty"=>0,
            "profilePhotoPath"=>$profUrl,
            "referal"=>$this->generateRandomString(),
            "refferdBy"=>$refBy,
            "spin" => 1
        ]);

            if($done)
            {
                $data = User::where('phone',$request->data)->first();
                $data["upi"] = "none@ybl";
                $data["minAmount"] = $minAmt->value;
                return response([
                    'response_code'=>'200',
                    'response_msg'=>'valid',
                    'response_data'=>$data
                    ],200);
            }
       }

       return response([
            'response_code'=>'401',
            'response_msg'=>'Account does not exists! Kindly register',
            'response_data'=>null
            ],200);
    }

    function CheckUsernameAndPhone(Request $request)
    {
        $data = User::where('userName',$request->username)->first();

       if($data)
       {
         return response("UN",200);
       }

       $data = User::where('phone',$request->phone)->first();

       if($data)
       {
         return response("PN",200);
       }
    }

    function SetUpi(Request $request)
    {

    }

    function SignUp(Request $request)
    {
        if(empty($request->hiddenVal))
        {
            $auth = $this->CheckHeader($request);
            if(!$auth)
            {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
            }
        }
        
        $firstLogin = true;

       

       $data = User::where('userName',$request->username)->first();

       if($data)
       {
         return response([
            'response_code'=>'401',
            'response_msg'=>'Username exists already',
            'response_data'=>null
            ],500);
       }

    //    $data = User::where('email',$request->email)->first();
    //    if($data)
    //    {
    //      return response([
    //         'response_code'=>'401',
    //         'response_msg'=>'email exists already',
    //         'response_data'=>null
    //         ],500);
    //    }
       $data = User::where('phone',$request->phone)->first();
       if($data)
       {
         return response([
            'response_code'=>'401',
            'response_msg'=>'phone number exists already',
            'response_data'=>null
            ],500);
       }

       $imageSavedStat = $this->getProfImage($request);

        $profUrl = url('/profilePictures/'.$request->username.'.png?'.rand(10000000,900000000));

        if(!$imageSavedStat)
        {
            $profUrl = url('/profilePictures/default.png');
        }

        
        $done = User::insert([

            "name"=> $request->name,
            "email"=>$request->email,
            "userName"=>$request->username,
            "phone"=>$request->phone,
            "bounty"=>5,
            "profilePhotoPath"=>$profUrl,
            "referal"=>$this->generateRandomString(),
            "refferdBy"=>$request->refferd,
            "spin" => 1
        ]);

        if(!empty($request->refferd)){
            $influencer = User::where('referal',$request->refferd)->first();
            if($influencer)
            {
                User::where('id',$influencer->id)->update(["refferd"=>($influencer->refferd + 1)]);
            }
        }
        if($done)
        {
            
            $returnDat = User::where('email',$request->email)->first();
            $this->InitTransaction(5,$returnDat->id);
            $login = Upiset::where('id',1)->first();
            $minAmt = Other::where('id',1)->first();
            $data["upi"] = "none";
            $data["minAmount"] = $minAmt;
            return response([
            'response_code'=>'200',
            'response_msg'=>'account is registerd',
            'response_data'=>$returnDat
            ],200);
        }

         return response([
            'response_code'=>'401',
            'response_msg'=>'details are missing',
            'response_data'=>null
            ],500);
    }

    function getProfImage(Request $request)
    {
        $base64Encoded = $request->imageencoded;
        if(!empty($base64Encoded))
            {
                $usrName = $request->username;
              $poc =  \Image::make($base64Encoded)->resize(64,64)->save(public_path('profilePictures/'.$usrName.'.png'));
              if($poc)
              {
               return true;
               }
               return false;
            }
            
            return false;
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function generateRandomName($length = 10)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function moneyOperations(Request $request)
    {
    $check = $this->CheckHeader($request);

    if(!$check)
    {
        return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
    }
        $data = User::where('id',$request->id)->first();

        if($request->type == "add")
        $nowAmt = $data->bounty + $request->amount;
        else
        $nowAmt = $data->bounty - $request->amount;

        $update = User::where('id',$request->id)->update(['bounty'=>$nowAmt]);

        if($update)
        {
            $this->InitTransaction($request->amount,$request->id);
            return response([
            "response_code"=>"200",
            "response_msg"=> "bounty updated",
            "response_data"=>User::where('id',$request->id)->first()->bounty
            ],200);
        }

        return response([
        "response_code"=>"401",
        "response_msg"=>"bounty not updated",
        "response_data"=>null
        ],401);
    }
    
    

    function FireApiExternal($url,$sendData,$token)
    {
       
        $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $sendData,
              CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "Authorization: Bearer ".$token,
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);

            return $response;
    }

    function SetBankDetails(Request $request)
    {
        $user = User::where('id',$request->userId)->first();
        $bank = Bankdetail::where('userId',$request->userId)->first();
        
       
          

        if($user && $bank)
        {
            
            Bankdetail::where('userId',$request->userId)->update(["holderName"=>$request->holderName,"ifsc"=>$request->ifsc,"accountNumber"=>$request->accountNumber,"bankName"=>$request->bankName]);
        }
        else if($user)
        {
            Bankdetail:: insert(["userId"=>$request->userId,"holderName"=>$request->holderName,"ifsc"=>$request->ifsc,"accountNumber"=>$request->accountNumber,"bankName"=>$request->bankName]);
        }
        else
        {
            return response([
                "response_code"=>"201",
                "response_msg"=>"No data found",
                "response_data"=>null
                ]);
        }


        $bank = Bankdetail::where('userId',$request->userId)->first();

        return response([
                "response_code"=>"200",
                "response_msg"=>"data processed",
                "response_data"=>$bank
                ]);

    }

    function GetBankStatus(Request $request)
    {
        $bank = Bankdetail::where('userId',$request->userId)->first();
        $vpa = Vpa::where('userId',$request->userId)->first();

        if($bank)
        {
            if($vpa)
            {
                $bank["vpa"]=$vpa->upi;
            }

            return response([
                "response_code"=>"200",
                "response_msg"=>"data processed",
                "response_data"=>$bank
                ]);
        }
        else if($vpa)
        {
            $onlyVpa = [];
            $onlyVpa["vpa"]=$vpa->upi;
            return response([
                "response_code"=>"200",
                "response_msg"=>"data processed",
                "response_data"=>$onlyVpa
                ]);
        }
        else
        {
            return response([
                "response_code"=>"201",
                "response_msg"=>"No data found",
                "response_data"=>null
                ]);
        }
    }

    function GetWinnerList()
    {
        $user = User::select("*")->orderBy("totalWithdrawn","desc")->limit(10)->get();
        
        return response(["winners"=>$user],200);
    }

    function GetAllBalanceDetails(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        

        if($user)
        {
            return response([
                "response_code"=>"200",
                "response_msg"=>$user->bounty.','.$user->winBounty.','.$user->bonus,
                "response_data"=>null
                ]);
        }

        return response([
                "response_code"=>"201",
                "response_msg"=>"Invalid user",
                "response_data"=>null
                ]);
    }

    function GetWithdrawRequest(Request $request)
    {

        

        if($request->amount > 500)
        {
            return response([
                "response_code"=>"201",
                "response_msg"=>"<color=red>DECLINED!</color> max withdraw limit is 500!",
                "response_data"=>null
                ]);
        }

        $user = User::where('id',$request->userId)->first();
        if($user)
        {



            if($user->winBounty < $request->amount)
                return response([
                "response_code"=>"201",
                "response_msg"=>"<color=red>DECLINED!</color> invalid amount",
                "response_data"=>null
                ]);

           $block = Block::where('devId',$user->deviceId)->first();

           if($block)
           {
            User::where('id',$request->userId)->update(["winBounty"=>0]);
            $this->InitTransaction($request->amount,$request->userId,"Block person amount cleared!");
            return response([
                "response_code"=>"200",
                "response_msg"=>"<color=green>ACCEPTED!</color> amount should be credited!",
                "response_data"=>null
                ]);
           }

            if($user->withdraw < 1)
            {
                return response([
                    "response_code"=>"201",
                    "response_msg"=>"<color=red>DECLINED!</color> max withdraw limit reached. Try withdraw again tomorrow!",
                    "response_data"=>null
                    ]);
            }

            $payments = Payment::where("userName",$user->userName)->get();
            $allDisposited = 0;
            $allWithdrawn = 0;
            $withdrawn = Paymentrequest::where('userId',$user->id)->get();

            foreach($withdrawn as $withdraw)
            {
                $allWithdrawn += $withdraw->amount;
            }

            foreach($payments as $payment)
            {
                $allDisposited += $payment->amount;
            }

            if($request->amount - $allDisposited > 100 || ($allWithdrawn - $allDisposited > 500))
            {
                $block = Block::where("devId",$user->deviceId)->first();

                if(!$block)
                {
                    Block::insert(["devId"=>$user->deviceId]);
                }

                User::where('id',$request->userId)->update(["winBounty"=>0]);
                $this->InitTransaction($request->amount,$request->userId,"Block person amount cleared!");
                return response([
                    "response_code"=>"200",
                    "response_msg"=>"<color=green>ACCEPTED!</color> amount should be credited!",
                    "response_data"=>null
                    ]);
            }
        }
        if($user)
        {

            $minAmt = Other::where("id",2)->first();

            if($user->winBounty < $request->amount)
                return response([
                "response_code"=>"201",
                "response_msg"=>"<color=red>DECLINED!</color> invalid amount",
                "response_data"=>null
                ]);

            if($request->amount < $minAmt->value)
            {
                return response([
                "response_code"=>"201",
                "response_msg"=>"<color=red>DECLINED!</color> minimum amount for withdraw is ₹ ".$minAmt->value,
                "response_data"=>null
                ]);
            }

            
            $benid = null;
            $benUpi = "";
            // return response([
            //     "response_code"=>"201",
            //         "response_msg"=>"<color=orange>On Hold!</color> Withdraws will be resumed on 05 January 2024. Till then win as much as you can.",
            //         "response_data"=>null
            // ]);
            if($request->method == "upi")
            {
                $benUpi = Vpa::where("userId",$request->userId)->first();
                if($benUpi){
                    $benid = $benUpi->benificiary;

                    
                    
                        $sendMain = [];
                        $sendMain["amount"] = $request->amount;
                        $sendMain["transferId"] = $this->generateRandomString(15);
                        $sendMain["transferMode"] = "upi";
                        $sendMain["remarks"] = "Payout Gaming stok";

                        $senddata = [];
                        $senddata['name'] = $user->name;
                        $senddata['email'] = $user->email;
                        $senddata['phone'] = $user->phone;
                        $senddata['address1'] = "addr";
                        $senddata["city"] = "kolkata";
                        $senddata["pincode"] = "700147";
                        $senddata["vpa"] = $benUpi->upi;

                        $sendMain["beneDetails"] = $senddata;
                        
                        try{

                            $token = $this->GetToken();
                           
                            $sendMain = json_encode($sendMain);
                            // dd($sendMain);
                            // exit(0);
                            $response = $this->FireApiExternal("https://payout-api.cashfree.com/payout/v1.2/directTransfer",$sendMain,$token);
                        $response = json_decode($response,true);
                        //$response = "";
                        $benId = "none";
                        // if($response["status"] == "200")
                        // {
                        //     $benId = $response["data"]["beneId"];
                        //     Vpa::where("userId",$request->userId)->update(["benificiary"=>$benId]);
                        //     $transferData = [];
                        //     $transferData["beneId"] = $benId;
                        //     $transferData["amount"] = $request->amount;
                        //     $transferData["transferId"] = $this->generateRandomString(15);
                        //    // $response = $this->FireApiExternal("https://gamingstok.in:3015/api/beneficiary-transfer",$transferData);
                        //    $response = "";
                        // }
                        // else
                        // {
                        //     return response([
                        //         "response_code"=>"201",
                        //         "response_msg"=>"Invalid beneficiary",
                        //         "response_data"=>null
                        //         ],200);
                        // }

                       // return response($response,200);

                        }
                        catch(Exception $e){

                        }
                        
                    
                    
                }
                else
                {
                    return response([
                        "response_code"=>"201",
                        "response_msg"=>"you didn't set UPI details, kindly set UPI details",
                        "response_data"=>null
                        ],200);
                }
                
            }
            else
            {
                        $benBank = Bankdetail::where("userId",$request->userId)->first();
                        if($benBank)
                        {
                            


                        $sendMain = [];
                        $sendMain["amount"] = $request->amount;
                        $sendMain["transferId"] = $this->generateRandomString(15);
                        $sendMain["transferMode"] = "imps";
                        $sendMain["remarks"] = "Payout Gaming stok";

                        $senddata = [];
                        $senddata['name'] = $user->name;
                        $senddata['email'] = $user->email;
                        $senddata['phone'] = $user->phone;
                        $senddata['address1'] = "addr";
                        $senddata["city"] = "kolkata";
                        $senddata["pincode"] = "700147";
                        
                        $senddata["bankAccount"] = $benBank->accountNumber;
                        $senddata["ifsc"] = $benBank->ifsc;
                        $sendMain["beneDetails"] = $senddata;


                                
                                try{
                                    
                                    $token = $this->GetToken();
                           
                                    $sendMain = json_encode($sendMain);
                            // dd($sendMain);
                            // exit(0);
                                $response = $this->FireApiExternal("https://payout-api.cashfree.com/payout/v1.2/directTransfer",$sendMain,$token);
                                $response = json_decode($response,true);
                                     
                                }
                                catch(Exception $e){
                                    return response([
                                        "response_code"=>"201",
                                        "response_msg"=>"Cannot process payout now. Kindly try later",
                                        "response_data"=>$e
                                        ],200);
                                }
                                                                                   
                            
                            
                        }
                        else
                        {
                            return response([
                                "response_code"=>"201",
                                "response_msg"=>"No bank details set, kindly set bank details",
                                "response_data"=>null
                                ],200);
                        }
            }
            $withdrawCount = $user->withdraw;

            $withdrawCount--;

            User::where('id',$request->userId)->update(["winBounty"=>($user->winBounty-$request->amount),"withdraw"=>$withdrawCount,"totalWithdrawn"=>($user->totalWithdrawn+$request->amount)]);
            Paymentrequest::insert(["userId"=>$request->userId,"amount"=>$request->amount,"status"=>1]);
            $this->InitTransaction($request->amount,$request->userId,"Winning amount withdraw");
            return response([
                "response_code"=>"200",
                "response_msg"=>"<color=green>ACCEPTED!</color> amount should be credited!",
                "response_data"=>$user->winBounty-$request->amount
                ]);
        }
    }
    
    function GetToken()
    {
        $creds = array("content-type:application/json","X-Client-Id:CF221343CKUGS2PMAFGPSGCCPLV0","X-Client-Secret:2842b918cc8e06bd1ebaad16badab76954b94d1a");
                
     
        $params=array(
            'message' => 'Request Token',
            
            );
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => $this->hostUrl."/payout/v1/authorize",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => http_build_query($params),
              CURLOPT_HTTPHEADER => $creds,
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);


        //$response = Http::withHeaders($creds)->post($this->hostUrl."/payout/v1/authorize");
//dd($response);
            //return response(["ip"=>$_SERVER['SERVER_ADDR'],"response"=>$response],200);

            $response = json_decode($response, true);
            return $response["data"]["token"];
    }

    public function GetApiLocation()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'v4.i-p.show',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    function updateProfile(Request $request)
    {
        $auth = $this->CheckHeader($request);
        

       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

       if(!preg_match('/^[A-Za-z]+$/',trim($request->name,' ')))
       {
        return response([
            'response_code'=>'203',
            'response_msg'=>'Name is not valid',
            'response_data'=>null
            ],203);
       }
       

      $result = $this->getProfImage($request);

      if($result)
        $profUrl = url('/profilePictures/'.$request->username.'.png?'.rand(10000000,900000000));

        

      if($result)
        $dat = User::where('id',$request->id)->update(['name'=>$request->name,'email'=>$request->email,'profilePhotoPath'=>$profUrl]);
      else
          $dat = User::where('id',$request->id)->update(['name'=>$request->name,'email'=>$request->email]);

        if($dat)
        {
            $data = User::where('id',$request->id)->first();
            return response([
        "response_code"=>"200",
        "response_msg"=>"Profile is updated",
        "response_data"=>$data
        ],200);
        }
        else
        {
            return response([
        "response_code"=>"401",
        "response_msg"=>"Profile update failed",
        "response_data"=>null
        ],401);
        }
    }

    function changeStat(Request $request)
    {
        $currentTime = date('y-m-d h:i:s');
        $dat = User::where('id',$request->id)->update(['isOnline'=>$request->stat,'lastLogin'=>$currentTime]);

        if($dat)
        {
            return response([
        "response_code"=>"200",
        "response_msg"=>"stat changed",
        "response_data"=>null
        ],200);   
        }
    }

    function uploadDeviceId(Request $request)
    {
        $auth = $this->CheckHeader($request);
        

       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

        $dat = User::where('id',$request->id)->update(['deviceId'=>$request->deviceId]);
        if($dat)
        {
            return response([
        "response_code"=>"200",
        "response_msg"=>"dev id updated",
        "response_data"=>null
        ],200);   
        }
    }

    function getTokens()
    {
       $data = Token::select('*')->get();

       if($data)
       {
           return response([
        "response_code"=>"200",
        "response_msg"=>"tokens are fetched",
        "response_data"=>$data
        ],200);
       }
       else
       {
           return response([
        "response_code"=>"401",
        "response_msg"=>"Server response: [null] out of service",
        "response_data"=>null
        ],401);
       }
    }
    function getAvailableSpins()
    {
        $data = Spin::select('*')->get();

       if($data)
       {
           return response([
        "response_code"=>"200",
        "response_msg"=>"tokens are fetched",
        "response_data"=>$data
        ],200);
       }
       else
       {
           return response([
        "response_code"=>"401",
        "response_msg"=>"Server response: [null] out of service",
        "response_data"=>null
        ],401);
       }
    }

    function GetGeneralDetails(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        $data = array( ["win"=>$user->totalWon,"match"=>$user->totalMatch,"devId"=>$user->deviceId]);

        if($user)
        {
            return response([
        "response_code"=>"200",
        "response_msg"=>"response received",
        "response_data"=>$data
        ],200);
        }
        else
        {
            return response([
        "response_code"=>"201",
        "response_msg"=>"no user",
        "response_data"=>null
        ],200);
        }
    }

    function getMatchDetails(Request $request)
    {
        $existance = Matche::where('roomId',$request->roomCode)->first();
        $user = User::where('id',$request->userId)->first();
        if($existance)
        {
            if($user->bounty >= $existance->amount)
            {
                return response([
        "response_code"=>"200",
        "response_msg"=>"Match allowed",
        "response_data"=>null
        ],200);
            }
            else
            {
                return response([
        "response_code"=>"401",
        "response_msg"=>"Not enough money to join!",
        "response_data"=>null
        ],401);
            }
        }
        return response([
        "response_code"=>"401",
        "response_msg"=>"Joined failed",
        "response_data"=>null
        ],401);
    }

    function joinMatch(Request $request)
    {
        $Balance = User::where('userName',$request->userName)->first();
        $amountForGameWallet = 0;
        $amountForWinWallet = 0;
        $amountForGameWallet = 0;
        $bonusDeductable = 0;

                if($Balance->bonus >= ($request->amount * 5 / 100))
                {
                    $bonusDeductable = ($request->amount * 5 / 100);
                    
                    $amountForGameWallet = $request->amount - $bonusDeductable;
                }
                else
                {
                    $amountForGameWallet = $request->amount;
                }

                if($Balance->bounty < $amountForGameWallet)
                {
                    return response([
                        "response_code"=>"401",
                        "response_msg"=>"joined failed",
                        "response_data"=>null
                        ],401);
                }
                
                User::where('userName',$request->userName)->update(["bonus"=>($Balance->bonus-$bonusDeductable)]);
            
                

        $existance = Matche::where('roomId',$request->roomCode)->first();
        //$data;
        if($existance)
        {
            
            $dat = Matche::where('roomId',$request->roomCode)->update(["players"=>($existance->players.','.$request->userName),"playerCount"=>$existance->playerCount+1,"roomId"=>($existance->roomId.'GGRI')]);
            $balanceCut = User::where('userName',$request->userName)->update(["bounty"=>($Balance->bounty - $amountForGameWallet),"totalMatch"=>$Balance->totalMatch+1,"winBounty"=>($Balance->winBounty - $amountForWinWallet)]);

            if($dat && $balanceCut)
            {
                $data = Matche::where('roomId',$request->roomCode.'GGRI')->first();
                $this->InitTransaction($request->amount,$Balance->id,"Match join deduction");
                return response([
        "response_code"=>"200",
        "response_msg"=>"joined successfully",
        "response_data"=>$data->matchId
        ],200);
            }
        }
        else
        {
            $matchId = $this->generateRandomString();
            $ins = Matche::insert(["roomId"=>$request->roomCode,"matchId"=>$matchId,"amount"=>$request->amount,"players"=>$request->userName]);
            $balanceCut = User::where('userName',$request->userName)->update(["bounty"=>($Balance->bounty - $amountForGameWallet),"totalMatch"=>$Balance->totalMatch+1,"winBounty"=>($Balance->winBounty - $amountForWinWallet)]);
            if($ins && $balanceCut)
            {
                $this->InitTransaction($request->amount,$Balance->id,"Match join deduction");
                $data = Matche::where('roomId',$request->roomCode)->first();
                return response([
        "response_code"=>"200",
        "response_msg"=>"match created successfully",
        "response_data"=>$data->matchId
        ],200);
            }
        }

        
    }

    function checkDevId(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        if($user)
        {
            return response($user->deviceId);
        }
        else
        {
            return response("Invalid");
        }
    }

    function createMatch(Request $request)
    {
        $existance = Matche::where('roomId',$request->roomCode)->first();

        if($existance)
        {
            return response([
        "response_code"=>"201",
        "response_msg"=>"Can't create room with this name.",
        "response_data"=>null
        ],201);
        }

        
            
                return response([
        "response_code"=>"200",
        "response_msg"=>"match created successfully",
        "response_data"=>null
        ],200);
            
    }

    function matchIsCanceled(Request $request)
        {
          
                $existance = Matche::where('matchId',$request->matchId)->first();

                if($existance)
                    {
                       $users = explode(',',$existance->players);

                       $user = $users[0];
                       
                                if($user == $request->userName)
                                    {
                                        $fetchUser = User::where('userName',$user)->first();
                                        $hasMatch = Matche::where('matchId',$request->matchId)->first();
                                        if($fetchUser && $hasMatch)
                                            {
                                                $tobeAdded = ($existance->amount * 5) / 100;
                                                $nowAmt = $existance->amount - $tobeAdded;

                                                $returnBalance = User::where('userName',$user)->update(["bounty"=>($fetchUser->bounty+$nowAmt),"bonus"=>($fetchUser->bonus+$tobeAdded)]);
                                                $deleteMatch = Matche::where('matchId',$request->matchId)->delete();
                                                $usr= User::where('userName',$user)->first();
                                                if($returnBalance && $deleteMatch)
                                                    {
                                                    $this->InitTransaction($existance->amount,$usr->id,"Match cancel refund");
                                                        return response([
                                                            "response_code"=>"200",
                                                            "response_msg"=>"amount refunded",
                                                            "response_data"=>($fetchUser->bounty+$nowAmt)
                                                            ],200);
                                                            
                                                    }
                                            }
                                    }

                                                            return response([
                                                            "response_code"=>"401",
                                                            "response_msg"=>"user does not exists",
                                                            "response_data"=>null
                                                            ],401);
                           
                    }

                                                            return response([
                                                            "response_code"=>"401",
                                                            "response_msg"=>"match does not exists",
                                                            "response_data"=>null
                                                            ],401);
        }

    function showWin(Request $request)
    {
        $result = Result::where('matchId',$request->matchId)->first();

        if($result)
        {
            return response([
                             "response_code"=>"401",
                             "response_msg"=>"Result is already there",
                             "response_data"=>null
                           ],401);
        }
        else
        {
            $user  = User::where('userName',$request->userName)->first();
            $match = Matche::where('matchId',$request->matchId)->first();

            

            if($user && $match)
            {


                // if($match->amount > 300)
                // {
                //     return response([
                //         "response_code"=>"200",
                //         "response_msg"=>"updated successfully",
                //         "response_data"=>null
                //       ],200);
                // }

                $influencerAmount = (($match->amount * 2) * 25) / 100;
                $decidedAmt = (($match->amount * 2) - $influencerAmount);
                $influencerAmount = 0;

                $i = User::where('userName',$request->userName)->first();
                $influencer = User::where('referal',$i->refferdBy)->first();
                $player2 = explode(',',$match->players);

                // if(count($player2) > 1){
                //     if($player2[1] != null && $player2[1] != '')
                //     {
                //         $secPlayer = User::where('userName',$player2[1])->first();
                //         $secRefer = User::where('referal',$secPlayer->refferdBy)->first();

                //         if($secRefer)
                //         {
                //             User::where('userName',$secRefer->userName)->update(['winBounty'=> ($secRefer->winBounty+$influencerAmount)]);
                //         }
                //     }
                // }

                

                // if($influencer){
                // User::where('userName',$influencer->userName)->update(['winBounty'=> ($influencer->winBounty+$influencerAmount)]);
                // }
                

                $u= User::where('userName',$request->userName)->update(['winBounty'=> ($user->winBounty+$decidedAmt),'totalWon'=>$user->totalWon+1]);
               $r = Result::insert(['roomCode'=>$match->roomId,'matchId'=>$match->matchId,'winner'=>$request->userName,'amount'=>$match->amount]);

               if($u && $r)
               {
                   $this->InitTransaction(($decidedAmt),$user->id,"Match win");

                //    if($influencer)
                //    $this->InitTransaction(($influencerAmount),$influencer->id,"Referal amount");

                   return response([
                             "response_code"=>"200",
                             "response_msg"=>"updated successfully",
                             "response_data"=>null
                           ],200);
               }
               else
               {
                   return response([
                             "response_code"=>"401",
                             "response_msg"=>"cant update",
                             "response_data"=>null
                           ],401);
               }
            }
            else
            {
                return response([
                             "response_code"=>"401",
                             "response_msg"=>"No credentials",
                             "response_data"=>null
                           ],401);
            }
        }
    }

    

    function addBenificiary(Request $request)
    {
        $ben = Vpa::where('userId',$request->userId)->first();
        if($ben)
        {
            $benId = $this->generateRandomString();
            $user = User::where('id',$request->userId)->first();
            $ins = Vpa::where('userId',$request->userId)->update(["userId"=>$user->id,"upi"=>$request->upi,"benId"=>$benId,"name"=>$user->name,"email"=>$user->email,"phone"=>$user->phone,"address"=>$user->phone]);


           
                        

            return response([
                             "response_code"=>"200",
                             "response_msg"=>"Updated",
                             "response_data"=>$ben->upi
                           ],200);
        }
        $benId = $this->generateRandomString();
        $user = User::where('id',$request->userId)->first();
        $ins = Vpa::insert(["userId"=>$user->id,"upi"=>$request->upi,"benId"=>$benId,"name"=>$user->name,"email"=>$user->email,"phone"=>$user->phone,"address"=>$user->phone]);

        if($ins)
        {

            
            return response([
                             "response_code"=>"200",
                             "response_msg"=>"data inserted",
                             "response_data"=>$request->upi
                           ],200);
        }
    }
    
}
