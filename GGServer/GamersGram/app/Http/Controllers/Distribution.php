<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contestrank;

class Distribution extends Controller
{
   public function Distribute($amount,$people,$contestId=null)
   {
    // Total amount to distribute
$totalAmount = $amount;

// Number of people
$numPeople = $people;

// Initialize variables
$remainingAmount = $totalAmount;
$distribution = [];

// Calculate distribution percentages
$firstPercentage = 10;
$secondPercentage = 5;

// Calculate amounts for the first and second person
$firstAmount = $totalAmount * ($firstPercentage / 100); // $300,000
$remainingAmount -= $firstAmount; // $2,700,000 remaining after giving $300,000 to the first person

$secondAmount = $totalAmount * ($secondPercentage / 100); // $150,000
$remainingAmount -= $secondAmount; // $2,550,000 remaining after giving $150,000 to the second person

$distribution[1] = $firstAmount; // Store $300,000 for the first person
$distribution[2] = $secondAmount; // Store $150,000 for the second person

// Loop to distribute the remaining amount among the rest of the people
for ($i = 3; $i <= $numPeople; $i++) {
    $percentage = $firstPercentage / $i; // Calculate the decreasing percentage for subsequent people
    
    // Calculate the amount based on the decreasing percentage
    $amount = $totalAmount * ($percentage / 100);
    
    // Check if the remaining amount is less than the calculated amount for the current person
    if ($remainingAmount < $amount) {
        $amount = $remainingAmount; // Set the remaining amount for the last person
    }
    
    $remainingAmount -= $amount; // Reduce the remaining amount after distributing to a person
    $distribution[$i] = $amount; // Store the calculated amount for the current person
}

// Output the distribution
//echo "Distribution:\n";

$prizeStr="";
$totalValue = 0;
$count = 0;
$minVal = 11;
$maxVal = 0;
$toAvg = 0;
$lastRank = 0;
$leftOver = ($numPeople - 10);
$devideWith = 0;

if($leftOver >= 100)
{
    $devideWith = 100;
}
else if($leftOver >= 10)
{
    $devideWith = 10;
}
else
{
    $devideWith = 1;
}

$grouping =round(($leftOver/$devideWith)) ;

for ($i = 1; $i <= $numPeople; $i++) {
    if(round($distribution[$i]) <= 0)
    {
        break;
    }

    if($i <= 10){
       // echo "Person $i gets: ₹" . round($distribution[$i]) ."<br/>";
       $prizeStr .= $i.":".round($distribution[$i]).'|';
       $lastRank = $i;
    }
    else{
        if($count >= $grouping)
        {
            $toAvg += round($distribution[$i]);
            //echo "Person $minVal to $maxVal gets: ₹" . round($toAvg/300) ."<br/>";
            $toAvg /= $grouping;
            $toAvg = round($toAvg);
            if($i < $numPeople)
                $prizeStr .= $minVal.'-'.$maxVal.':'.$toAvg.'|';
            else
                $prizeStr .= $minVal.'-'.$maxVal.':'.$toAvg;
            $count = 0;
            $minVal = $maxVal;
            $lastRank = $maxVal;
            $toAvg = 0;
            
        }
        else{

            $toAvg += round($distribution[$i]);
            $maxVal = $i;
            $count++;
        }

    }
    $totalValue += round($distribution[$i]);
    
}

//echo("Total vals: ".$totalValue);
//return $prizeStr;
if($contestId != null)
Contestrank::insert(["contestId"=>$contestId,"lastRank"=>$lastRank,"allRankPrizes"=>$prizeStr]);
else
    return $prizeStr;
   }

   
}
