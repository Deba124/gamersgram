<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\AdminCheck;
use App\Http\Controllers\Logout;
use App\Http\Controllers\Activities;
use App\Http\Controllers\Distribution;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    header("location:gamersgram.com");
});
Route::get('admin', function () {
    return view('Admin/Pages/adminlogin');
});
Route::get('dashboard', function () {
    return view('Admin/Pages/dashboard');
});
Route::get('servicedown', function () {
    return view('Admin/Pages/serviceDown');
});
Route::get('users', function () {
    return view('Admin/Pages/users');
});
Route::get('transaction', function () {
    return view('Admin/Pages/transactions');
});
Route::get('activity', function () {
    return view('Admin/Pages/activityPage');
});
Route::get('match', function () {
    return view('Admin/Pages/matchesView');
});
Route::get('paymentStat', function () {
    return view('Admin/Pages/paymentDetails');
});
Route::get('userTran', function () {
    return view('Admin/Pages/userTransaction');
});

Route::get('approve', function () {
    return view('verify');
});

Route::get('webRegistration', [Users::class,'webRegistration']);

Route::get('/checkdb',[Users::class,'DbCheck']);
Route::post('setPayment',[Users::class,'SetPaymentNow']);
Route::get('/checkIns',[Users::class,'InitTransaction']);
Route::get('logout',[Logout::class,'getOut']);
Route::post("adminCheck",[AdminCheck::class,'CheckAdmin']);
Route::post("block",[Activities::class,'blockUser']);
Route::get("unblock",[Activities::class,'unblockUser']);
Route::get("activate",[Activities::class,'enableOrDiableToken']);
Route::post("updateSpin",[Activities::class,'updateSpinValues']);
Route::post("addbots",[Activities::class,'AddBots']);
Route::get('promote',[Users::class,'promoteUser']);
Route::get('demote',[Users::class,'demoteUser']);
Route::get("acceptPayment",[Activities::class,'AcceptPayment']);
Route::get("rejectPayment",[Activities::class,'DeclinePayment']);
Route::post('registerWeb',[Users::class,'SignUp']);
Route::post('add_money',[Users::class,'AddMoney']);
Route::post('add_money_withdraw',[Users::class,'AddMoneyWithdraw']);
Route::post('updateUpi',[Users::class,'UpdateUpi']);
Route::post('updateVersion',[Users::class,'UpdateVersion']);
Route::get("checkInit",[Users::class,'GamingStokRummy']);
Route::get('getPayoutToken',[Users::class,'GetToken']);
Route::get('getApiLocation',[Users::class,'GetApiLocation']);
Route::get('updateWithdraw',[Activities::class,'UpdateWithdraw']);
Route::get('searchUser',[Activities::class,'SearchUser']);
Route::get('distribute',[Distribution::class,'Distribute']);
Route::post('createContest',[Activities::class,'CreateContest']);
Route::post('addContestBot',[Activities::class,'AddUsersBulk']);
Route::get('geRand',[Activities::class,'GetRandBots']);
Route::get('getResult',[Users::class,'GetResult']);
Route::get('contestRefresh',[Users::class,'ContestRefresh']);
Route::get('publishResult',[Users::class,'PublishResult']);
Route::get('updateAllContest',[Users::class,'UpdateAllContest']);



