<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\Activities;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[Users::class,'Login']);
Route::post('register',[Users::class,'SignUp']);
Route::post('update',[Users::class,'updateProfile']);
Route::post('stat',[Users::class,'changeStat']);
Route::post('registerDevId',[Users::class,'uploadDeviceId']);
Route::get('tokens',[Users::class,'getTokens']);
Route::get('spins',[Users::class,'getAvailableSpins']);
Route::post('join',[Users::class,'joinMatch']);
Route::post('create',[Users::class,'createMatch']);
Route::post('canceled',[Users::class,'matchIsCanceled']);
Route::post('updateBalance',[Users::class,'moneyOperations']);
Route::post('setTemp',[Users::class,'setTempPay']);
Route::post('wallet',[Users::class,'getWalletUpdate']);
Route::post('matchDetails',[Users::class,'getMatchDetails']);
Route::post('win',[Users::class,'showWin']);
Route::post('addBenificary',[Users::class,'addBenificiary']);
Route::post('details',[Users::class,'GetGeneralDetails']);
Route::post('bankdetails',[Users::class,'SetBankDetails']);
Route::post('checkbank',[Users::class,'GetBankStatus']);
Route::post('withdraw',[Users::class,'GetWithdrawRequest']);
Route::post('allBalance',[Users::class,'GetAllBalanceDetails']);
Route::post('checkDevice',[Users::class,'checkDevId']);
Route::post('checkDevice',[Users::class,'checkDevId']);
Route::get('bots',[Users::class,'getBotsDetails']);
Route::get('version',[Users::class,'checkVersion']);
Route::get('getWinnerList',[Users::class,'GetWinnerList']);
Route::post('spin',[Users::class,'updateSpin']);
Route::post('trans',[Users::class,'GetUserTransactions']);
Route::post('elegible',[Users::class,'getElegibility']);
Route::post('userCheck',[Users::class,'CheckUsernameAndPhone']);
Route::post('registerOtp',[Users::class,'RegisterOtp']);
Route::post('whatsapp',[Users::class,'GetWhatsappData']);
Route::post('getRummyStat',[Users::class,'GetRummystat']);
Route::post('joinContest',[Users::class,'JoinContest']);
Route::post('getMyRanking',[Users::class,'GetMyRanking']);
Route::get('getAllContests',[Users::class,'GetAllContests']);
Route::get('getPastContests',[Users::class,'GetPastContests']);
//Route::post('getPayoutToken',[Users::class,'GetToken']);
