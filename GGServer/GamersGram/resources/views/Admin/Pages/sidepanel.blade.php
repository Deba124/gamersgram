<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{URL::asset('img/sidebar-1.jpg');}}">
<div class="logo"><a href="#" class="simple-text logo-normal">
          Ludo Time
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item" id="dashboard">
            <a class="nav-link" href="dashboard">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item" id="user">
            <a class="nav-link" href="users">
              <i class="material-icons">person</i>
              <p>Users</p>
            </a>
          </li>
          <li class="nav-item" id="transaction">
            <a class="nav-link" href="transaction">
              <i class="material-icons">library_books</i>
              <p>Transactions</p>
            </a>
          </li>

            <li class="nav-item" id="activity">
            <a class="nav-link" href="activity">
              <i class="material-icons">bubble_chart</i>
              <p>Activity</p>
            </a>
          </li>
          <li class="nav-item" id="match">
            <a class="nav-link" href="match">
              <i class="material-icons">content_paste</i>
              <p>Matches</p>
            </a>
          </li>
          <li class="nav-item" id="contest">
            <a class="nav-link" href="contest">
              <i class="material-icons">content_paste</i>
              <p>Contests</p>
            </a>
          </li>
        </ul>
      </div>
    </div>