<?php
session_start();
?>
<?php
if(!isset($_SESSION['login']))
{
    header("Location:admin");
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('apple-icon.png');}}">
  <link rel="icon" type="image/png" href="{{URL::asset('img/favicon.png');}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Contests
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
  <!-- CSS Files -->
  <link href="{{URL::asset('css/material-dashboard.css?v=2.1.2');}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{URL::asset('css/demo.css');}}" rel="stylesheet" />
  <script>
    $(document).ready(function () {
           // $("#manage").attr("class", "active");
            $("#contest").attr("class", "active");
            $("#contest").attr("href", "#");
        });
  </script>
</head>
<body>
<div class="wrapper ">
    @inlcude('Admin.Pages.sidepanel')
    </div>
</body>